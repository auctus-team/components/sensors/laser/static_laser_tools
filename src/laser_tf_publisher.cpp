#include "ros/ros.h"
#include "tf/transform_listener.h"
#include "sensor_msgs/PointCloud.h"
#include "sensor_msgs/PointCloud.h"
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include "laser_geometry/laser_geometry.h"
#include "visualization_msgs/Marker.h"
#include "geometry_msgs/PointStamped.h"
#include "std_msgs/Float32.h"

class LaserScanToPointCloud{

public:

  ros::NodeHandle n_;
  laser_geometry::LaserProjection projector_;
  tf::TransformListener listener_;
  message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub_;
  tf::MessageFilter<sensor_msgs::LaserScan> laser_notifier_;
  ros::Publisher scan_pub_ , human_end_eff_dist_pub , closest_point_pub;
  std::string root_link, tip_link;
  geometry_msgs::PointStamped closest_pt;
  tf::TransformListener tf_listener_;
  std_msgs::Float32 min_dist;
  Eigen::VectorXd last = Eigen::VectorXd::Zero(0);
  double filter_freq;
  LaserScanToPointCloud(ros::NodeHandle n) : 
    n_(n),
    laser_sub_(n_, "laser_tools/laserscan_min", 10),
    laser_notifier_(laser_sub_,listener_, "laser", 10)
  {
    laser_notifier_.registerCallback(boost::bind(&LaserScanToPointCloud::scanCallback, this, _1));
    laser_notifier_.setTolerance(ros::Duration(0.01));
    scan_pub_ = n_.advertise<sensor_msgs::PointCloud>("/laser_pc",1);
    human_end_eff_dist_pub = n_.advertise<std_msgs::Float32>("/Human_end_eff_laser_min_dist",1);
    closest_point_pub = n.advertise<geometry_msgs::PointStamped>("closest_laser_point",1);
    if (!n.getParam("/closest_point/root_link", root_link))
      ROS_ERROR_STREAM("You need to specify a robot radius has a ros param");
    if (!n.getParam("/closest_point/tip_link", tip_link))
      ROS_ERROR_STREAM("You need to specify a robot radius has a ros param");
    if (!n.getParam("/closest_point/filter_freq", filter_freq))
      ROS_ERROR_STREAM("You need to specify a filter frequency has a ros param");
  }

  void scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_in)
  {
    sensor_msgs::PointCloud cloud;
    try
    {
        projector_.transformLaserScanToPointCloud(
          root_link,*scan_in, cloud,listener_);
    }
    catch (tf::TransformException& e)
    {
        std::cout << e.what();
        return;
    }
    

    tf::StampedTransform end_eff_transform;
    try{
      std::string end_eff_frame_ = tip_link;
      tf_listener_.lookupTransform(root_link, end_eff_frame_, ros::Time(0.0), end_eff_transform);    
      
     
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      return;
    }
    
    // Find closest point to the end_eff_frame_
    double dist;
    min_dist.data = 10;
    
    for (int i=0; i<cloud.points.size() ;++i)
    {
        Eigen::Vector2d cloud_i(cloud.points[i].x ,cloud.points[i].y  );
        double dt = 0.01;
        Eigen::Vector2d cloud_i_filtered = filter(cloud_i,1.0/filter_freq,dt);
        dist =  std::sqrt(std::pow(end_eff_transform.getOrigin().x() - cloud_i_filtered[0] ,2)  + std::pow(end_eff_transform.getOrigin().y() - cloud_i_filtered[1] ,2) ); 
        if (dist < min_dist.data)
        {
            min_dist.data = dist;
            closest_pt.point.x = cloud.points[i].x;
            closest_pt.point.y = cloud.points[i].y;
            closest_pt.point.z = cloud.points[i].z;    
        }
    }

    closest_pt.header.frame_id = root_link;
    closest_pt.header.stamp = ros::Time::now();

    // Publish vector between point and end-effector

    human_end_eff_dist_pub.publish(min_dist);
    scan_pub_.publish(cloud);
    closest_point_pub.publish(closest_pt);
  }

  Eigen::VectorXd filter(Eigen::VectorXd cur, double w0, double dt) {
    if (last.rows() == 0) {
      last.resize(cur.rows(), 1);
      last.setZero();
    }

    if (w0 < 0) {
      return cur;
    }

    double beta = exp(-w0 * dt);
    Eigen::VectorXd filtered = beta * last + (1 - beta) * cur;
    last = cur;
    return filtered;
  } 
};




int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "my_scan_to_cloud");
  ros::NodeHandle n;
  LaserScanToPointCloud lstopc(n);
  
  ros::spin();
  
  return 0;
}
