 # Continous integration

[![pipeline status](https://gitlab.inria.fr/auctus/panda/static_laser_tools/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/panda/static-laser-tools/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=auctus:panda:static-laser-tools)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:static-laser-tools)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:static-laser-tools&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:static-laser-tools)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:static-laser-tools&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:static-laser-tools)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:static-laser-tools&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:static-laser-tools)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:static-laser-tools&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:static-laser-tools)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:static-laser-tools&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:static-laser-tools)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:static-laser-tools&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:static-laser-tools)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Astatic-laser-tools

<!-- - Socumentation : https://tlaine.gitlabpages.inria.fr/test-kombo-server/

# Keyence

Package for reading the keyence SZV-32N laser scanner data and publish them as a ROS sensors_msgs/LaserScan Message

To test the code:
`roslaunch keyence keyence.launch`

Optional parameters :
- hostname : the laser IP adress
- port : the port on which to send/listen for the udp packet

The resulting laser message is published on the topic /scan
 -->

# static laser tool

Package for processing a laser scan message